﻿namespace DangerReport.Core.Models
{
    using System;

    public class Statuses
    {
        public string Status { get; set; }

        public string Description { get; set; }

        public string ChangeDate { get; set; }
    }
}