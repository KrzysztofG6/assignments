﻿namespace DangerReport.Core.Models
{
    public class Notification
    {
        public string Category { get; set; }

        public string City { get; set; }

        public string Subcategory { get; set; }

        public string District { get; set; }

        public string AparmentNumber { get; set; }

        public string Street2 { get; set; }

        public string NotificationType { get; set; }

        public string CreateDate { get; set; }

        public string SiebelEventId { get; set; }

        public string Source { get; set; }

        public double YCoordOracle { get; set; }

        public string Street { get; set; }

        public string DeviceType { get; set; }

        public Statuses[] Statuses { get; set; }

        public string XCoordOracle { get; set; }

        public string NotificationNumber { get; set; }

        public double YCoordWGS84 { get; set; }

        public string Event { get; set; }

        public double XCoordWGS84 { get; set; }
    }
}