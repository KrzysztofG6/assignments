﻿namespace DangerReport.Core.ViewModels
{
    public class DangerReportViewModel
    {
        public string Category { get; set; }

        public string City { get; set; }

        public string Subcategory { get; set; }

        public string District { get; set; }

        public string Event { get; set; }
    }
}