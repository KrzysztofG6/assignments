﻿namespace DangerReport.Core.Infrastructure.Vendor.Data
{
    using System.Collections.Generic;
    using Models;

    public interface IWarsawDangersDataProvider
    {
        List<Notification> GetDangersReport();
    }
}