﻿namespace DangerReport.Infrastructure.Mapper
{
    using AutoMapper;
    using Profiles;

    public class MapperFactory
    {
        public IMapper Create()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ModelsToDtoProfile>();
            });

            return config.CreateMapper();
        }
    }
}