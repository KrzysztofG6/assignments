﻿namespace DangerReport.Infrastructure.Mapper.Profiles
{
    using AutoMapper;
    using Core.Models;
    using Core.ViewModels;

    public class ModelsToDtoProfile : Profile
    {
        public ModelsToDtoProfile()
        {
            CreateMap<Notification, DangerReportViewModel>();
        }
    }
}