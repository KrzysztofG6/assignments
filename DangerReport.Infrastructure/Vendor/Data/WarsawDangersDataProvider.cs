﻿namespace DangerReport.Infrastructure.Vendor.Data
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Core.Infrastructure.Vendor.Data;
    using Core.Models;
    using Newtonsoft.Json.Linq;

    public class WarsawDangersDataProvider : IWarsawDangersDataProvider
    {
        private static readonly string UrlRequestPart = "action/19115store_getNotificationsForDistrict?id=28dc65ad-fff5-447b-99a3-95b71b4a7d1e&district=Wola&apikey=a0885c8e-c51e-4f15-8250-6d499e3860db";

        private readonly HttpClient _httpClient;

        public WarsawDangersDataProvider(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public List<Notification> GetDangersReport()
        {
            var data = _httpClient.GetAsync($"{ConfigurationManager.AppSettings["WarsawApiUrl"]}{UrlRequestPart}").Result;
            var content = data.Content.ReadAsStringAsync().Result;
            var jsonData = JObject.Parse(content);

            var notifications = jsonData["result"]["result"]["notifications"].Children().ToList();
            var results = new List<Notification>();


            foreach (var notification in notifications)
            {
                results.Add(notification.ToObject<Notification>());
            }

            return results;
        }
    }
}