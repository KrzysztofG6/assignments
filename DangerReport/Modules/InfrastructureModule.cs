﻿namespace DangerReport.Modules
{
    using Autofac;
    using DangerReport.Infrastructure.Vendor.Data;

    public class InfrastructureModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<WarsawDangersDataProvider>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            base.Load(builder);
        }
    }
}