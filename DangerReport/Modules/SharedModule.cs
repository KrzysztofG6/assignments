﻿namespace DangerReport.Modules
{
    using System;
    using System.Net.Http;
    using Autofac;
    using AutoMapper;
    using Infrastructure.Mapper;

    public class SharedModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(x =>
                {
                    var client = new HttpClient();
                    client.Timeout = TimeSpan.FromMinutes(2);
                    return client;
                })
                .As<HttpClient>()
                .SingleInstance();

            builder.Register(x => new MapperFactory().Create())
                .As<IMapper>()
                .InstancePerLifetimeScope();

            base.Load(builder);
        }
    }
}