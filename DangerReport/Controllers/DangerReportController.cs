﻿using System.Web.Mvc;

namespace DangerReport.Controllers
{
    using System.Collections.Generic;
    using AutoMapper;
    using Core.Infrastructure.Vendor.Data;
    using Core.ViewModels;

    public class DangerReportController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IWarsawDangersDataProvider _warsawDangersDataProvider;

        public DangerReportController(IWarsawDangersDataProvider warsawDangersDataProvider,
            IMapper mapper)
        {
            _warsawDangersDataProvider = warsawDangersDataProvider;
            _mapper = mapper;
        }

        public ActionResult Data()
        {
            var data = _warsawDangersDataProvider.GetDangersReport();
            var model = _mapper.Map<List<DangerReportViewModel>>(data);

            return PartialView("DangerReportData", model);
        }
    }
}