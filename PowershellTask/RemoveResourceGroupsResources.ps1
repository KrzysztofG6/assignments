﻿param([string]$ResourceGroup = "PredicaAssignment", [string[]]$ResourceTypes = ("Microsoft.Web/sites", "Microsoft.Sql/servers"))
az login #For build server token should be used
az account set --subscription "Free Trial"; #Specific for my env

$ResourcesInGroup = az resource list --resource-group $ResourceGroup | ConvertFrom-Json

for($i = 0; $i -lt $ResourceTypes.Length; $i++){
    $ResourceType = $ResourceTypes.get($i);

    for($j = 0; $j -lt $ResourcesInGroup.Length; $j++){
        $Resource = $ResourcesInGroup.Get($j);

            if($Resource.type -eq $ResourceType){
                az resource delete -n $Resource.name -g $ResourceGroup --resource-type $ResourceType               
            }
        }
    }

Write-Host "Resource group cleared!"